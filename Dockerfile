FROM python:3
ADD . /proxy
WORKDIR /proxy
# ENV DNS_SERVER_IP       1.1.1.1
# ENV DNS_SERVER_PORT     853
# ENV DNS_SERVER_NAME     cloudflare-dns.com
# ENV CA_PATH             /etc/ssl/certs/ca-certificates.crt
# ENV LISTENING_IP        0.0.0.0
# ENV LISTENING_PORT      5353
ENTRYPOINT ["python", "./proxy.py"]
